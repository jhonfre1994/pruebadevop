/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.devops.ejemplo.service.impl;

import com.devops.ejemplo.repository.PersonRepository;
import static org.junit.jupiter.api.Assertions.assertEquals;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.modelmapper.ModelMapper;
import org.springframework.boot.test.context.SpringBootTest;

/**
 *
 * @author jhonfre
 */
public class PersonImplTest {

    private PersonRepository personRepository;
    private ModelMapper mapper;
    PersonImpl personImpl;

    public PersonImplTest() {
    }

    @BeforeEach
    public void setUp() {
        mapper = new ModelMapper();
        personRepository = Mockito.mock(PersonRepository.class);
        personImpl = new PersonImpl(personRepository, mapper);
    }

    @Test
    public void probarTestSi(){
        int res = personImpl.pruebTest(1);
        assertEquals(1, res);
    }
    
    @Test
    public void probarTestNo(){   
        int res = personImpl.pruebTest(2);
        assertEquals(0, res);
    }
    
    
    @Test
    public void probarTestSi2(){
        int res = personImpl.pruebTest2(1);
        assertEquals(1, res);
    }
    
    @Test
    public void probarTestNo2(){   
        int res = personImpl.pruebTest2(2);
        assertEquals(0, res);
    }

    
    @Test
    public void probarTestSi3(){
        int res = personImpl.pruebTest3(1);
        assertEquals(1, res);
    }
    
    @Test
    public void probarTestNo3(){   
        int res = personImpl.pruebTest3(2);
        assertEquals(0, res);
    }
    
}
